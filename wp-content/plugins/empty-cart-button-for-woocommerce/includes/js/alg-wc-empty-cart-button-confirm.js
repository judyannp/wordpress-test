/**
 * Empty Cart Button for WooCommerce - jQuery Confirm
 *
 * @version 1.2.4
 * @since   1.2.4
 * @author  Algoritmika Ltd.
 * @see     https://craftpip.github.io/jquery-confirm/
 * @todo    [dev] use `jc-attached` instead of `alg-wc-empty-cart-button-confirm-bind`
 * @todo    [feature] customizable styling (e.g. `theme`) etc.
 */

function alg_wc_ecb_confirm() {
	jQuery( '.alg-wc-empty-cart-button-confirm' ).each( function() {
		if ( ! jQuery( this ).hasClass( 'alg-wc-empty-cart-button-confirm-bind' ) ) {
			jQuery( this ).addClass( 'alg-wc-empty-cart-button-confirm-bind' );
			jQuery( this ).confirm( {
				title: false,
				content: alg_wc_ecb_confirm_object.content,
				theme: 'material',
				boxWidth: '300px',
				useBootstrap: false,
			} );
		}
	} );
}

jQuery( document ).ready( function() {
	alg_wc_ecb_confirm();
	jQuery( document ).ajaxComplete( alg_wc_ecb_confirm );
} );
