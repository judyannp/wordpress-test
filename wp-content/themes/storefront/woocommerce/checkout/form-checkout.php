<?php
/**
 * Checkout Form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/form-checkout.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.5.0
 */

 global $woocommerce;
 #print_r($woocommerce);

if ( ! defined( 'ABSPATH' ) ) {
  exit;
}

global $wpdb, $woocommerce, $post, $product;

$results = $wpdb->get_results("SELECT * FROM wp_posts WHERE post_status = 'publish' AND post_type = 'product'");

function woocommerce_ajax_add_to_cart_js() {
  if (function_exists('is_product') && is_product()) {
      wp_enqueue_script('woocommerce-ajax-add-to-cart', plugin_dir_url(__FILE__) . 'assets/ajax-add-to-cart.js', array('jquery'), '', true);
  }
}
add_action('wp_enqueue_scripts', 'woocommerce_ajax_add_to_cart_js', 99);
?>


<!--  -->

<?php
//$res = get_post_meta($product->id);

do_action( 'woocommerce_before_checkout_form', $checkout );

// If checkout registration is disabled and not logged in, the user cannot checkout.
if ( ! $checkout->is_registration_enabled() && $checkout->is_registration_required() && ! is_user_logged_in() ) {
  echo esc_html( apply_filters( 'woocommerce_checkout_must_be_logged_in_message', __( 'You must be logged in to checkout.', 'woocommerce' ) ) );
  return;
}


?>


    <button type="button" id="add-me" name="add-to-cart" value="47" class="single_add_to_cart_button button alt"  onclick="myFunction();" >Add to cart</button>

<form name="add-to-cart" method="post">
  <input type="text" name="" id="product_id">
  <button type="submit" name="add-to-cart" id="add-to-cart" value="" class="single_add_to_cart_button button alt">add to cart</button>
</form>

<?php
add_action( 'woocommerce_cart_coupon', 'woocommerce_empty_cart_button' );
function woocommerce_empty_cart_button() {
	echo '<a href="' . esc_url( add_query_arg( 'empty_cart', 'yes' ) ) . '" class="button" title="' . esc_attr( 'Empty Cart', 'woocommerce' ) . '">' . esc_html( 'Empty Cart', 'woocommerce' ) . '</a>';
}

if ( isset( $_GET['empty_cart'] ) && 'yes' === esc_html( $_GET['empty_cart'] ) ) {
  WC()->cart->empty_cart();

  //$referer  = wp_get_referer() ? esc_url( remove_query_arg( 'empty_cart' ) ) : wc_get_cart_url();
  //wp_safe_redirect( $referer );
}
?>

<form name="checkout" method="post" class="checkout woocommerce-checkout" action="<?php echo esc_url( wc_get_checkout_url() ); ?>" enctype="multipart/form-data">

  <?php if ( $checkout->get_checkout_fields() ) : ?>

    <?php do_action( 'woocommerce_checkout_before_customer_details' ); ?>

    <div class="col2-set" id="customer_details">
      <div class="col-1">
        <?php do_action( 'woocommerce_checkout_billing' ); ?>
      </div>

      <div class="col-2">
        <?php do_action( 'woocommerce_checkout_shipping' ); ?>
      </div>
    </div>

    <?php do_action( 'woocommerce_checkout_after_customer_details' ); ?>

  <?php endif; ?>
  
  <?php do_action( 'woocommerce_checkout_before_order_review_heading' ); ?>
  
  <h3 id="order_review_heading"><?php esc_html_e( 'Your order', 'woocommerce' ); ?></h3>
  
  <?php do_action( 'woocommerce_checkout_before_order_review' ); ?>

  <div id="order_review" class="woocommerce-checkout-review-order">
    <?php do_action( 'woocommerce_checkout_order_review' ); ?>
  </div> 

  <?php do_action( 'woocommerce_checkout_after_order_review' ); ?>

</form>

<?php do_action( 'woocommerce_after_checkout_form', $checkout ); ?>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>  
<script>
function myFunction(){
  
  // $.get("get_product_details.php", function(products){
  //   alert("Data: " + data + "\nStatus: " + status);
  // });
  // var product_id = $("#product_id").val();

  // $.get( "http://localhost/wordpress-test/product/ASD123/", { product_id: product_id} )
  // .done(function( data ) {
  // });

  console.log("hey");
  alert("hey");
  let data = [{"name":"quantity","value":"1"},{"name":"add-to-cart","value":"57"}];

  $.ajax({
      type: 'POST',
      url: "http://localhost/wordpress-test/?wc-ajax=add_to_cart",
      data: data,
      // beforeSend: function (response) {
      //   $thisbutton.removeClass('added').addClass('loading');
      // },
      // complete: function (response) {
      //   $thisbutton.addClass('added').removeClass('loading');
      // },
      success: function (response) {
        console.log(response);
        if (response.error & response.product_url) {
          window.location = response.product_url;
          alert("hey");
          return;
        }

        setTimeout(function(){ 
          
      //     $.ajax({
      // type: 'POST',
      // url: "http://localhost/wordpress-test/hey.php",
      // data: ,
      // success: function (response) {
      //     alert("hry");
      //     console.log(response);
      // },
      // error: function (response) {
      //   alert(response);
      // }

         // });
          jQuery(document.body).trigger("update_checkout");

          //
           ///?wc-ajax=get_refreshed_fragments
      //   $.ajax({
      // type: 'POST',
      // url: "http://localhost/wordpress-test/?wc-ajax=get_refreshed_fragments",
      // success: function (response) {
      //   console.log(response);
      //   if (response.error & response.product_url) {
      //     window.location = response.product_url;
      //     return;
      //   }
      // }
      //   });


      //?empty-cart
      let data = {"cart_item_key":"57"};
        $.ajax({
      type: 'POST',
      url: "http://localhost/wordpress-test/?wc-ajax=clear_my_cart",
      data: data,
      success: function (response) {
        console.log(response);
        alert("hey11");
        if (response.error & response.product_url) {
          window.location = response.product_url;
          return;
        }
      }
        });



        }, 300);

       

        //$(document.body).trigger('added_to_cart', [response.fragments, response.cart_hash]);
      },
    });


}
</script>
