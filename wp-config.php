<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'sample' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'H3zn_>|tLXmMY1Oc8j&3XL?AV){#[/6UhnOFR^;{S;9+QPwPEvBiKZPj%Zi}i(vY' );
define( 'SECURE_AUTH_KEY',  'KsPF[VS+6*]A3FjT??(|)`wh/(cogVG@98{nt9uv3!X6e}N1bfu9cX+ &?!;<oaF' );
define( 'LOGGED_IN_KEY',    '-B%8thr~cJ(Wf Osd4siqNP%qz#q9-?LYS>mcQ!-<_;SN{LI*R;1m%?N6)$A!^ +' );
define( 'NONCE_KEY',        '9_-9,DqI$+*c%Bw5}a-_%T3}B.*9PS2w]j?RIyP?f/SJxjbQo,WPr#;X?;~#u6 c' );
define( 'AUTH_SALT',        '&>?VVGA4Q?e%>+}!Yo3/8n= /[l@V%k9$lbbuP!{e!f*dtm*WqRB}w!g~c%|r^6a' );
define( 'SECURE_AUTH_SALT', '#r)eP:Yc?cy8;w<7;t.KfLkQe7FzX[.O/tqxyMA2o.8&a1/?h-}tHh5!/yZEpH[$' );
define( 'LOGGED_IN_SALT',   'w6Cfr0(8JI)$_Ea*6OQJ^MLiFeCYFyJ6%.K4Z,$eDL=K h2?*:i%)$EG%5ExbH8H' );
define( 'NONCE_SALT',       '8nJy8spA<qs/nKty67X3?I8%FoZatQKp5.?Ym,hlrvo!i@5FIP;%WSq_!&!7t3[>' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
